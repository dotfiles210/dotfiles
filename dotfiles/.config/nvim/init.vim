call plug#begin ('~/local/share/nvim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tomasiser/vim-code-dark'
Plug 'joshdick/onedark.vim'
Plug 'preservim/nerdtree'
Plug 'jiangmiao/auto-pairs'
Plug 'preservim/nerdcommenter'
call plug#end()

set background=dark
colorscheme onedark

let g:airline_theme='atomic'
let g:airline#extensions#tabline#enabled = 1 
let g:airline#extensions#branch#enabled = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.dirty='⚡'

"Basic settings
set number
syntax on
set encoding=utf-8
set expandtab
set tabstop=1
set softtabstop=1
set shiftwidth=4
set clipboard+=unnamedplus
set nocompatible

nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
vmap ++ <plug>NERDCommenterToggle
nmap ++ <plug>NERDCommenterToggle
"nnoremap <C-c> :call NERDComment<CR>
"nnoremap <silent> <leader>c{ V{:call NERDComment('x', 'toggle')<CR>
"let NERDTreeMapOpenInTab='<ENTER>'
