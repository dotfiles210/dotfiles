--vim.cmd [[packadd packer.nvim]]
local set = vim.opt
local g = vim.g

set.number      = true
set.expandtab   = true
set.syntax      = "on"
set.tabstop     = 1
set.softtabstop = 1
set.shiftwidth  = 4
set.clipboard   = "unnamedplus"
set.fileencoding= "utf-8"

--GitGutter auto enable
g.gitgutter_enabled = 1
