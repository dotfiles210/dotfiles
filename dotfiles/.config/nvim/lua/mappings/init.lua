-- toggle NERDTree show/hide using <C-n> and <leader>n
vim.api.nvim_set_keymap("n", "<C-t>", ":NERDTreeToggle<CR>", {noremap = true})
-- reveal open buffer in NERDTree
vim.api.nvim_set_keymap("n", "tt", ":NERDTreeFind<CR>", {noremap = true})

--cycle through buffers
vim.api.nvim_set_keymap("n", "gb", ":bnext<CR>", {noremap = true})
vim.api.nvim_set_keymap("n", "gB", ":bprevious<CR>", {noremap = true})

--Toggle NERDCommenter
vim.api.nvim_set_keymap("n", "++", "<plug>NERDCommenterToggle", {noremap = false})
vim.api.nvim_set_keymap("v", "++", "<plug>NERDCommenterToggle", {noremap = false})

--Toggle telescope to find files within the directory
vim.api.nvim_set_keymap("n", "tT", ":Telescope find_files<CR>", {noremap = true})
--Toggle telescope to find current buffer 
vim.api.nvim_set_keymap("n", "tB", ":Telescope current_buffer_fuzzy_find<CR>", {noremap = true})
