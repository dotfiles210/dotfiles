--local home = os.getenv('HOME')
  local db = require('dashboard')
  -- macos
  --db.preview_command = 'cat | lolcat -F 0.3'
  -- linux
  --db.preview_command = 'ueberzug'
  --
  --db.preview_file_path = home .. '/'
  --db.preview_file_height = 20
  --db.preview_file_width = 70
  db.custom_center = {
      {icon = '  ',
      desc = 'Find  File                              ',
      action = 'Telescope find_files find_command=rg,--hidden,--files',
      shortcut = 'SPC f f'},

      {icon = '  ',
      desc ='File Browser                            ',
      action =  'Telescope find_files',
      shortcut = 'SPC f b'},

      {icon = '  ',
      desc = 'Find  word                              ',
      action = 'Telescope live_grep',
      shortcut = 'SPC f w'},

      --{icon = '  ',
      --desc = 'Edit Neovim config                  ',
      --action = 'e ~/.config/nvim/init.lua',
      --shortcut = 'SPC f d'},
    }


--vim.o.dashboard_custom_section = {
  --a = {description = {'  Find File          '}, command = 'Telescope find_files'},
  --d = {description = {'  Search Text        '}, command = 'Telescope live_grep'},
  --b = {description = {'  Recent Files       '}, command = 'Telescope oldfiles'},
  --e = {description = {'  Config             '}, command = 'edit ~/.config/nvim/init.lua'}
--}
--vim.o.dashboard_custom_footer = {'Do one thing, do it well - Unix Philosophy'}
